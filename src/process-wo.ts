/** Begin Helpers * */

/** This is Brendan Brownlow's Westland Code Screening Test * */

/** You may write any number of helper functions here ( or in other files, should you wish) * */
import { fetchWorkOrders, getContact } from './apis/fetch-wo';
import { WorkOrder } from './types/types';

/**
 * An important helper to determine if you're a talented and gifted developer!
 */
export const areYouAwesome = () : boolean => true;

/**
 * Gives an array value based on the contact information
 * @param email The string value of the email to lookup
 */
 export const getArrayValue = (email : String) => {
  switch (email) {
    case 'g.freeman@blackmesa.com':
      return 0;
    case 'g.man@hotmail.com':
      return 1;
    case 'feldspar@outerwilds.th':
      return 2;
    case 'solanum@eye.nm':
      return 3;
    case 'm.okiura@abis.go.jp':
      return 4;
    case 'a.set@lemniscate.jp':
      return 5;
    case 'c.johnson@aperture.science':
      return 6;
    case 'm.mario@mushroom.kd':
      return 7;
    default:
      return 8;
  }
}

/** End Helpers * */

/**
 * The function is what will be called by main.ts. The data you return here will be consumed by an
 * email service that will send each point of contact the list of work orders that have been assigned to them.
 * You can return this data in any format you think suits the above. Be sure to read the readme for further requirements.
 *
 * Free to change the type of your return!
 */
export const processWorkOrders = async () : Promise<WorkOrder[][]> => {
  const workOrders: WorkOrder[] = await fetchWorkOrders();
  
  //workOrders sorted into 9 categories
  const sortedOrders: WorkOrder[][] = [[], [], [], [], [], [], [], [], []];

  /**
   * Iterates through the array 
   */
  for (let i = 0; i < workOrders.length; i++)
  {
    //Ignores invalid workOrders
    if (workOrders[i].part % 10 == 1 || workOrders[i].part % 10 == 7)
    {
      continue;
    }

    //Pushes work order onto correct corresponding array
    const val = getArrayValue(getContact(workOrders[i].department, workOrders[i].part));
    sortedOrders[val].push(workOrders[i]);
  }

  //Returns the double array of workorders for next part to parse
  return sortedOrders;
};
