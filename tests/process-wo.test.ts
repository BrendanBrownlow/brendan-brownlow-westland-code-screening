import { areYouAwesome, processWorkOrders, getArrayValue } from '../src/process-wo';
import { WorkOrder } from '../src/types/types';

/**
 * Perhaps the world's most important test
 *
 * Validates the expected handling of `areYouAwesome`
 * against yourself
 */
test('Are you an awesome developer?', () => {
  const isAnAwesomeDev = areYouAwesome();
  expect(isAnAwesomeDev).toBe(true);
});


/**
 * Tests helper function
 * 
 * Validates all values return correctly
 */
test('Helper function working', () => {
  const isZero = getArrayValue('g.freeman@blackmesa.com');
  expect(isZero).toBe(0);
  const isOne = getArrayValue('g.man@hotmail.com');
  expect(isOne).toBe(1);
  const isTwo = getArrayValue('feldspar@outerwilds.th');
  expect(isTwo).toBe(2);
  const isThree = getArrayValue('solanum@eye.nm');
  expect(isThree).toBe(3);
  const isFour = getArrayValue('m.okiura@abis.go.jp');
  expect(isFour).toBe(4);
  const isFive = getArrayValue('a.set@lemniscate.jp');
  expect(isFive).toBe(5);
  const isSix = getArrayValue('c.johnson@aperture.science');
  expect(isSix).toBe(6);
  const isSeven = getArrayValue('m.mario@mushroom.kd');
  expect(isSeven).toBe(7);
  const isEight = getArrayValue('it.help@westlandreg.com');
  expect(isEight).toBe(8);
})
